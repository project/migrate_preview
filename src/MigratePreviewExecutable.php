<?php

namespace Drupal\migrate_preview;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Exception\RequirementsException;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutable as MigrateExecutableBase;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\RequirementsInterface;
use Drupal\migrate\Row;

/**
 * Defines a migrate executable for previewing.
 */
class MigratePreviewExecutable extends MigrateExecutableBase {

  use MessengerTrait;

  /**
   * Maximum number of items to process in this migration.
   *
   * 0 indicates no limit is to be applied.
   *
   * @var int
   */
  protected $itemLimit = 0;

  /**
   * Counter of map saves, used to detect the item limit threshold.
   *
   * @var int
   */
  protected $itemLimitCounter = 0;

  /**
   * {@inheritdoc}
   */
  public function __construct(MigrationInterface $migration, MigrateMessageInterface $message = NULL, array $options = []) {
    parent::__construct($migration, $message);
    if (isset($options['limit'])) {
      $this->itemLimit = $options['limit'];
    }
  }

  /**
   * Get the ID map from the current migration.
   *
   * @return \Drupal\migrate\Plugin\MigrateIdMapInterface
   *   The ID map.
   */
  protected function getIdMap() {
    return $this->migration->getIdMap();
  }

  /**
   * Fetches and parses data that can be used to preview a migration.
   *
   * @return \Drupal\migrate\Row[]
   *   A list of migrate rows.
   */
  public function preview() {
    $this->getEventDispatcher()->dispatch(new MigrateImportEvent($this->migration, $this->message), MigrateEvents::PRE_IMPORT);

    // Knock off migration if the requirements haven't been met.
    try {
      if ($this->migration instanceof RequirementsInterface) {
        $this->migration->checkRequirements();
      }
    }
    catch (RequirementsException $e) {
      $this->messenger()->addWarning(
        $this->t(
          'Migration @id did not meet the requirements. @message @requirements',
          [
            '@id' => $this->migration->id(),
            '@message' => $e->getMessage(),
            '@requirements' => $e->getRequirementsString(),
          ]
        ),
      );
    }

    $source = $this->getSource();
    $id_map = $this->getIdMap();
    $id_map->prepareUpdate();

    try {
      $source->rewind();
    }
    catch (\Exception $e) {
      $this->messenger()->addError(
        $this->t('Migration failed with source plugin exception: @e', ['@e' => $e->getMessage()])
      );
      return [];
    }

    $rows = [];
    while ($source->valid()) {
      $row = $source->current();
      assert($row instanceof Row);
      $this->sourceIdValues = $row->getSourceIdValues();

      try {
        $this->processRow($row);
        $rows[] = $row;
        $this->itemLimitCounter++;

        if ($this->itemLimit != 0 && $this->itemLimitCounter >= $this->itemLimit) {
          break;
        }
      }
      catch (MigrateException $e) {
        // Show a warning or error to the user, depending on the error level.
        switch ($e->getLevel()) {
          case MigrationInterface::MESSAGE_ERROR:
            $level = MessengerInterface::TYPE_ERROR;
            break;

          case MigrationInterface::MESSAGE_WARNING:
          case MigrationInterface::MESSAGE_NOTICE:
            $level = MessengerInterface::TYPE_WARNING;
            break;

          case MigrationInterface::MESSAGE_INFORMATIONAL:
            $level = MessengerInterface::TYPE_STATUS;
            break;

          default:
            $level = MessengerInterface::TYPE_ERROR;
            break;
        }
        $this->messenger()->addMessage($e->getMessage(), $level);
      }
      catch (MigrateSkipRowException $e) {
        // Not an error. Show an informational message to the user when there is
        // a message available.
        if ($message = trim($e->getMessage())) {
          $data = [];
          foreach ($this->sourceIdValues as $key => $value) {
            $data[] = $this->t('@label: @value', [
              '@label' => $key,
              '@value' => $value,
            ]);
          }
          $this->messenger()->addStatus($this->t('Row with "@data" is skipped: @message', [
            '@data' => implode(', ', $data),
            '@message' => $message,
          ]));
        }
      }
      catch (\Exception $e) {
        $this->messenger()->addError(
          $this->t('Migration failed with source plugin exception: @e', ['@e' => $e->getMessage()])
        );
      }

      try {
        $source->next();
      }
      catch (\Exception $e) {
        $this->messenger()->addError(
          $this->t('Migration failed with source plugin exception: @e', ['@e' => $e->getMessage()])
        );
        return $rows;
      }
    }

    return $rows;
  }

}
