# Migrate Preview

The Migrate Preview module allows you to see how your content to migrate looks
like without having to execute an actual migration.

## How it works
It displays the information in two tables:

- Source data
  This the data that came from the source, though it is not the raw source data.
  Depending on the migrate source plugin, additional data can have been added.
  Process plugins have been applied. They can cause certain rows to be removed
  from the source, but are expected to not alter the data in other ways.
- Processed data
  This is the data that would be written to the destination when the migration
  is executed. Process plugins have been applied.

## Notes
- For each table, only the first 10 records will be displayed. This limit may be
  configurable in the future. Right now, this number is hard-coded in
  `MigrationController::preview()`.
- A preview does not interrupt an existing migration in process.
