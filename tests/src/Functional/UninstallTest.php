<?php

namespace Drupal\Tests\migrate_preview\Functional;

/**
 * Tests module uninstallation.
 *
 * @group migrate_preview
 */
class UninstallTest extends MigratePreviewBrowserTestBase {

  /**
   * Tests module uninstallation.
   */
  public function testUninstall() {
    // Confirm that Migrate Preview has been installed.
    $module_handler = $this->container->get('module_handler');
    $this->assertTrue($module_handler->moduleExists('migrate_preview'));

    // Uninstall Migrate Preview.
    $this->container->get('module_installer')->uninstall(['migrate_preview']);
    $this->assertFalse($module_handler->moduleExists('migrate_preview'), 'Migrate Preview is uninstalled.');
  }

}
