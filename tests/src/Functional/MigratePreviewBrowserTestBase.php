<?php

namespace Drupal\Tests\migrate_preview\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides a base class for Migrate Preview functional tests.
 */
abstract class MigratePreviewBrowserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'migrate_preview',
  ];

  /**
   * A test user with administrative privileges.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an user with Feeds admin privileges.
    $this->adminUser = $this->drupalCreateUser([
      'administer migrations',
    ]);
    $this->drupalLogin($this->adminUser);
  }

}
