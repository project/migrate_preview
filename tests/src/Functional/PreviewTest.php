<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_preview\Functional;

use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\VocabularyInterface;

/**
 * Testing the preview page.
 *
 * @group migrate_preview
 */
class PreviewTest extends MigratePreviewBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'csv_source_test',
    'user',
    'filter',
    'field',
    'node',
    'text',
    'taxonomy',
    'migrate',
    'migrate_plus',
    'migrate_preview',
    'migrate_preview_csv_source_test',
    'migrate_source_csv',
    'migrate_tools',
    'migrate_tools_test',
  ];

  /**
   * A taxonomy vocabulary.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  private VocabularyInterface $vocabulary;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->vocabulary = $this->createVocabulary([
      'vid' => 'fruit',
      'name' => 'Fruit',
    ]);
  }

  /**
   * Tests previewing a migration of embedded data.
   */
  public function testPreviewEmbeddedData() {
    $this->drupalGet('/admin/structure/migrate/manage/default/migrations/fruit_terms/preview');

    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->pageTextContains('Preview');
    $session->pageTextContains('Source data');
    $session->pageTextContains('Processed data');
    $session->pageTextContains('Apple');
    $session->pageTextContains('Banana');
    $session->pageTextContains('Orange');
  }

  /**
   * Tests previewing a migration that throws an exception.
   */
  public function testPreviewWithSourceException() {
    $this->drupalGet('/admin/structure/migrate/manage/default/migrations/source_exception/preview');

    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->pageTextContains('Preview');
    $session->pageTextContains('Source data');
    $session->pageTextContains('Processed data');
    $session->pageTextContains('Migration failed with source plugin exception');
  }

  /**
   * Tests previewing a migration with a CSV source.
   */
  public function testPreviewCsvSource() {
    // The source data for this test.
    $source_data = <<<'EOD'
vid,name,description,hierarchy,weight
tags,Tags,Use tags to group articles,0,0
forums,Sujet de discussion,Forum navigation vocabulary,1,0
test_vocabulary,Test Vocabulary,This is the vocabulary description,1,0
genre,Genre,Genre description,1,0
EOD;
    $this->writeSourceData('test.csv', $source_data);

    $this->drupalGet('/admin/structure/migrate/manage/default/migrations/csv_source_test/preview');

    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->pageTextContains('Preview');
    $session->pageTextContains('Source data');
    $session->pageTextContains('Processed data');
    $session->pageTextContains('Use tags to group articles');
  }

  /**
   * Tests a migration where process plugins are applied.
   */
  public function testWithProcessPluginsApplied() {
    // The source data for this test.
    $source_data = <<<'EOD'
guid,title
1,"Lorem ipsum"
2,"Ut wisi enim ad minim veniam"
3,
EOD;
    $this->writeSourceData('test.csv', $source_data);

    $this->drupalGet('/admin/structure/migrate/manage/migrate_preview_csv_test/migrations/csv_source_process_plugins_test/preview');

    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->pageTextContains('Preview');
    $session->pageTextContains('Source data');
    $session->pageTextContains('Processed data');

    // Ensure that the static map plugin was applied to the first row.
    $session->pageTextContains('Lorem dolor');
    $session->pageTextContains('Ut wisi enim ad minim veniam');

    // One row should have been skipped and for that a message should be
    // displayed.
    $session->pageTextContains('Row with "guid: 3" is skipped: The title is empty.');
  }

  /**
   * Tests a migration where a process plugin is not configured correctly.
   */
  public function testWithInvalidProcessPluginConfig() {
    // The source data for this test.
    $source_data = <<<'EOD'
guid,title
1,"Lorem ipsum"
2,"Ut wisi enim ad minim veniam"
3,
EOD;
    $this->writeSourceData('test.csv', $source_data);

    $this->drupalGet('/admin/structure/migrate/manage/migrate_preview_csv_test/migrations/invalid_process_plugin_config/preview');

    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->pageTextContains('Preview');
    $session->pageTextContains('Source data');
    $session->pageTextContains('Processed data');

    // An error message should be displayed.
    $session->pageTextContains('explode: delimiter is empty');
  }

  /**
   * Writes source data to the file system.
   *
   * @param string $filename
   *   The name of the file in the public directory to write data to.
   * @param string $source_data
   *   The data to write.
   */
  protected function writeSourceData(string $filename, string $source_data) {
    // Setup the file system so we create the source.
    $this->container->get('stream_wrapper_manager')->registerWrapper('public', PublicStream::class, StreamWrapperInterface::NORMAL);
    $this->container->get('file_system')->mkdir('public://sites/default/files', NULL, TRUE);

    // Write the data to the filepath given in the test migration.
    file_put_contents('public://' . $filename, $source_data);
  }

  /**
   * Creates a custom vocabulary based on default settings.
   *
   * @param array $values
   *   An array of settings to change from the defaults.
   *   Example: 'vid' => 'foo'.
   *
   * @return \Drupal\taxonomy\VocabularyInterface
   *   Created vocabulary.
   */
  protected function createVocabulary(array $values = []): VocabularyInterface {
    // Find a non-existent random vocabulary name.
    if (!isset($values['vid'])) {
      do {
        $id = strtolower($this->randomMachineName(8));
      } while (Vocabulary::load($id));
    }
    else {
      $id = $values['vid'];
    }
    $values += [
      'id' => $id,
      'name' => $id,
    ];
    $vocabulary = Vocabulary::create($values);
    $status = $vocabulary->save();

    $this->assertSame($status, SAVED_NEW);

    return $vocabulary;
  }

}
