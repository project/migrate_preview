<?php

namespace Drupal\Tests\migrate_preview\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests module installation and uninstallation.
 *
 * @group migrate_preview
 */
class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [];

  /**
   * Module handler to ensure installed modules.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->moduleHandler = $this->container->get('module_handler');
    $this->moduleInstaller = $this->container->get('module_installer');
  }

  /**
   * Tests that the module is installable.
   */
  public function testInstallation() {
    // Install migrate_preview.
    $this->assertFalse($this->moduleHandler->moduleExists('migrate_preview'), 'Migrate Preview is already installed.');
    $this->assertTrue($this->moduleInstaller->install(['migrate_preview']));

    // Workaround https://www.drupal.org/node/2021959
    // See \Drupal\Core\Test\FunctionalTestSetupTrait::rebuildContainer.
    unset($this->moduleHandler);
    $this->rebuildContainer();
    $this->moduleHandler = $this->container->get('module_handler');

    $this->assertTrue($this->moduleHandler->moduleExists('migrate_preview'), 'Failed to install Migrate Preview.');
  }

}
